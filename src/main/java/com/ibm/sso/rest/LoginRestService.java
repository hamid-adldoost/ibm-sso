package com.ibm.sso.rest;

import com.ibm.sso.common.BusinessExceptionCode;
import com.ibm.sso.common.SecurityServiceException;
import com.ibm.sso.dao.SecurityRoleDao;
import com.ibm.sso.dto.*;
import com.ibm.sso.jwt.SecurityWrapper;
import com.ibm.sso.model.SecurityPermission;
import com.ibm.sso.model.SecurityRole;
import com.ibm.sso.model.SecurityUser;
import com.ibm.sso.security.PermissionManagerService;
import com.ibm.sso.service.SecurityPermissionService;
import com.ibm.sso.service.SecurityRoleService;
import com.ibm.sso.service.SecurityService;
import com.ibm.sso.service.SecurityUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/auth")
public class LoginRestService {

    private final SecurityService securityService;
    private final PermissionManagerService permissionManagerService;
    private final SecurityUserService securityUserService;
    private final SecurityRoleService securityRoleService;
    private final SecurityPermissionService securityPermissionService;

    @Autowired
    public LoginRestService(SecurityService securityService, PermissionManagerService permissionManagerService, SecurityUserService securityUserService, SecurityRoleService securityRoleService, SecurityPermissionService securityPermissionService) {
        this.securityService = securityService;
        this.permissionManagerService = permissionManagerService;
        this.securityUserService = securityUserService;
        this.securityRoleService = securityRoleService;
        this.securityPermissionService = securityPermissionService;
    }

    @PostMapping(path = "/login")
    public SecurityWrapper login(@RequestParam(name = "username") String username,
                                 @RequestParam(name = "password") String password,
                                 HttpServletResponse response) {

//        SecurityWrapper securityWrapper = securityService.authenticate(loginDto.getUsername(), loginDto.getPassword());
        SecurityWrapper securityWrapper = securityService.authenticate(username, password);
        response.setHeader(HttpHeaders.AUTHORIZATION, securityWrapper.getFreshToken());
        return securityWrapper;
    }

    @GetMapping(path = "/perms")
    public SecurityWrapper findPermissionsForUser(@RequestHeader(name = "jwt") String jwt) {
        return securityService.authenticate(jwt);
    }

    @GetMapping(path = "/{roleName}")
    public Set<String> findPermissionsForRoleName(
            @RequestHeader(name = "jwt") String jwt,
            @PathVariable(name = "roleName") String roleName) {
        SecurityWrapper securityWrapper = securityService.authenticate(jwt);
        if (!securityWrapper.getRoles().contains(roleName))
            throw new SecurityServiceException(BusinessExceptionCode.ACCESS_DENIED.name());
        Map<String, Set<String>> globalPermsMap = permissionManagerService.findPermissionsOfRoles();
        return globalPermsMap.get(roleName);
    }

    @GetMapping(path = "/update-admin")
    public void updateAdmin() {
        permissionManagerService.updateAdminRole();
    }

    @GetMapping(path = "/create-admin")
    public SecurityUserDto createAdmin() {
        return permissionManagerService.createAdminUser();
    }

    @GetMapping(path = "/create-system-user")
    public SecurityUserDto createSystemUser() {
        return permissionManagerService.createSystemUser();
    }

    @PostMapping(path = "/register-user")
    public SecurityUser registerUser(@RequestBody RegisterDto registerDto) {

        return securityService.registerUser(registerDto);
    }

    @GetMapping("/find-by-username/{username}")
    public RegisterDto findByUsername(@PathVariable(name = "username") String username) {
        SecurityUserDto securityUserDto = securityUserService.findUserByUsernameOrEmail(username, null);
        if (securityUserDto != null)
            return new RegisterDto(securityUserDto);
        return null;
    }

    @GetMapping("/permissions/{role}")
    public Set<String> findPermissionsOfRole(@PathVariable String role) {
        Set<String> permissions = new HashSet<>();
        if (role == null || role.isEmpty()) {
            List<SecurityPermissionDto> permissionList = securityPermissionService.findByExample(new SecurityPermissionDto());
            permissions = permissionList.stream().map(SecurityPermissionDto::getName).collect(Collectors.toSet());
        } else {
            List<SecurityRoleDto> roleDtoList = securityRoleService.findByExample(new SecurityRoleDto());
            for (SecurityRoleDto r : roleDtoList) {
                permissions.addAll(r.getPermissionList().stream().map(SecurityPermissionDto::getName).collect(Collectors.toList()));
            }
        }
        return permissions;
    }

    @PostMapping("/add-permissions")
    public void registerPermission(@RequestBody Set<String> permissions) {
        if (permissions == null || permissions.isEmpty())
            return;
        Set<String> existing = findPermissionsOfRole(null);
        if (!(existing == null || existing.isEmpty())) {
            List<String> repeatitives = new ArrayList<>();
            permissions.forEach(p -> {
                if (existing.contains(p)) {
                    repeatitives.add(p);
                }
            });
            if (!repeatitives.isEmpty()) {
                repeatitives.forEach(permissions::remove);
            }
        }
        if (permissions.size() > 0) {
            permissions.forEach(p -> {
                SecurityPermissionDto per = new SecurityPermissionDto();
                per.setName(p);
                per.setTitle(p);
                securityPermissionService.save(per);
            });
        }
    }

    @PostMapping(path = "/change-own-pass")
    public SecurityUserDto changePass(@RequestBody ChangePasswordDto changePasswordDto) {
        String username = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
        changePasswordDto.setUsername(username);
        changePasswordDto.setEmail(null);
        return securityService.changeOwnPass(changePasswordDto);
    }

}
