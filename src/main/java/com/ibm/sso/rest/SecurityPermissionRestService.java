package com.ibm.sso.rest;

import com.caracal.data.SortUtil;
import com.caracal.data.api.qbe.SortObject;
import com.caracal.data.api.qbe.StringSearchType;
import com.ibm.sso.dto.SecurityPermissionDto;
import com.ibm.sso.service.SecurityPermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.ibm.sso.service.PagedResult;import java.util.Date;

import org.springframework.security.access.annotation.Secured;
import com.ibm.sso.security.AccessRoles;
import java.text.ParseException;
import java.util.Collections;
import java.util.List;


/* Generated By AEF Generator ( Powered by Dr.Adldoost :D ) */

@RestController
@RequestMapping("/security-permission")
public class SecurityPermissionRestService {

    private final SecurityPermissionService securityPermissionService;

    @Autowired
    public SecurityPermissionRestService(SecurityPermissionService securityPermissionService) {
        this.securityPermissionService = securityPermissionService;
    }

    @PreAuthorize("hasAuthority('admin_authority')")
    @GetMapping("/{id}")
    public SecurityPermissionDto findById(@PathVariable(name = "id")Long id) {
        return securityPermissionService.findByPrimaryKey(id);
    }

    @PreAuthorize("hasAuthority('admin_authority')")
    @GetMapping("/search")
    public PagedResult search(
                                      @RequestParam(value = "name", required = false) String name,
                                      @RequestParam(value = "id", required = false) Long id,
                                      @RequestParam(value = "title", required = false) String title,
                                      @RequestParam(value = "firstIndex", required = false) Integer firstIndex,
                                      @RequestParam(value = "pageSize", required = false) Integer pageSize,
                                      @RequestParam(value = "sortField", required = false) String sortField,
                                      @RequestParam(value = "sortOrder", required = false) String sortOrder) {

            SortObject sortObject = SortUtil.generateSortObject(sortField, sortOrder);
            List<SortObject> sortObjectList = null;
            if(sortObject != null)
               sortObjectList = Collections.singletonList(sortObject);

            if(firstIndex == null)
               firstIndex = 0;
            if(pageSize == null)
               pageSize = Integer.MAX_VALUE;
            SecurityPermissionDto securityPermission = new SecurityPermissionDto();
            securityPermission.setName(name); 
            securityPermission.setId(id); 
            securityPermission.setTitle(title); 

            return securityPermissionService.findPagedByExample(securityPermission,
                   sortObjectList,
                   firstIndex,
                   pageSize,
                   StringSearchType.EXPAND_BOTH_SIDES,
                   null,
                   null
                   );
    }


    @PreAuthorize("hasAuthority('admin_authority')")
    @PostMapping(path = "/save")
    public SecurityPermissionDto save(@RequestBody SecurityPermissionDto securityPermission) {
        return securityPermissionService.save(securityPermission);
    }


    @PreAuthorize("hasAuthority('admin_authority')")
    @DeleteMapping(path = "/remove/{id}")
    public void remove(@PathVariable(name = "id")Long id) {
        securityPermissionService.remove(id);
    }
}