package com.ibm.sso.dao;

import com.caracal.data.impl.AbstractDAOImpl;
import com.ibm.sso.model.LoginRequest;
import org.springframework.stereotype.Repository;

@Repository
public class LoginRequestDao extends AbstractDAOImpl<LoginRequest, Long> {

    public LoginRequestDao() {
        super(LoginRequest.class);
    }

}
