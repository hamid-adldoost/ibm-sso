package com.ibm.sso.service;

import com.caracal.data.api.GenericEntityDAO;
import com.ibm.sso.dao.SecurityPermissionDao;
import com.ibm.sso.dto.SecurityPermissionDto;
import com.ibm.sso.model.SecurityPermission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SecurityPermissionService extends GeneralServiceImpl<SecurityPermissionDto, SecurityPermission, Long> {


    private final SecurityPermissionDao securityPermissionDao;

    @Autowired
    public SecurityPermissionService(SecurityPermissionDao securityPermissionDao) {
        this.securityPermissionDao = securityPermissionDao;
    }

    @Override
    protected GenericEntityDAO getDAO() {
        return securityPermissionDao;
    }

    @Override
    public SecurityPermissionDto getDtoInstance() {
        return new SecurityPermissionDto();
    }

    public List<SecurityPermissionDto> findPermissionsByNames(List<String> names) {
        return SecurityPermissionDto.toDto(securityPermissionDao.findPermissionsByNames(names));
    }
}
