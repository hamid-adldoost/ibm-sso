package com.ibm.sso.service;

import com.caracal.data.api.qbe.StringSearchType;
import com.ibm.sso.common.BusinessExceptionCode;
import com.ibm.sso.common.SecurityServiceException;
import com.ibm.sso.dao.LoginRequestDao;
import com.ibm.sso.dao.SecurityUserDao;
import com.ibm.sso.dto.*;
import com.ibm.sso.jwt.JWTUtil;
import com.ibm.sso.jwt.SecurityWrapper;
import com.ibm.sso.model.LoginRequest;
import com.ibm.sso.model.SecurityPermission;
import com.ibm.sso.model.SecurityRole;
import com.ibm.sso.model.SecurityUser;
import com.ibm.sso.model.enums.LoginResponseResult;
import com.ibm.sso.security.PermissionManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class SecurityService {

//    private final SecurityUserService userService;
    private final SecurityUserDao userDao;
    private final SecurityRoleService roleService;
    private final PasswordEncoder passwordEncoder;
    private final PermissionManagerService permissionManagerService;
    private final SecurityPermissionService permissionService;
    private final LoginRequestDao loginRequestDao;

    @Autowired
    SecurityService securityService;

    @Value("${password.regex}")
    private String passwordRegex;

    @Autowired
    public SecurityService(SecurityUserDao userDao, SecurityRoleService roleService, PermissionManagerService permissionManagerService, PasswordEncoder passwordEncoder, SecurityPermissionService permissionService, LoginRequestDao loginRequestDao) {
        this.userDao = userDao;
        this.roleService = roleService;
        this.passwordEncoder = passwordEncoder;
        this.permissionManagerService = permissionManagerService;
        this.permissionService = permissionService;
        this.loginRequestDao = loginRequestDao;
    }

//    public SecurityWrapper authenticate(String username, String password) {
//
//        //Authentication should be done here...
//        //Sample Authentication
//        if(username.equalsIgnoreCase("admin")
//                && password.equalsIgnoreCase("admin")) {
//            SecurityWrapper securityWrapper = new SecurityWrapper();
//            securityWrapper.setSecure(true);
//            securityWrapper.setUsername("admin");
//            securityWrapper.setPermissions(new ArrayList<>());
//            securityWrapper.setRoles(Collections.singletonList("user"));
//            return securityWrapper;
//        }
//        else
//            throw new SecurityServiceException(BusinessExceptionCode.ACCESS_DENIED.name());
//
//
//    }

    @Transactional(propagation= Propagation.REQUIRES_NEW)
    public void SaveLoginRequest(LoginRequest loginRequest) {

        loginRequestDao.save(loginRequest);
    }


    public SecurityWrapper authenticate(String username, String password) {

        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsername(username);
        loginRequest.setRequestDate(new Date());

        SecurityUser user = new SecurityUser();
        user.setUsername(username);
        user = userDao.findSingleByExample(user, StringSearchType.EXACT);
        if(user == null) {
            loginRequest.setResult(LoginResponseResult.UNSUCCESSFUL.ordinal());
            securityService.SaveLoginRequest(loginRequest);
            throw new SecurityServiceException(BusinessExceptionCode.ACCESS_DENIED.name());
        }
        if(user.getUsername().equalsIgnoreCase(username)) {
            if(!passwordEncoder.matches(user.getUsername() + password, user.getPassword())) {
                loginRequest.setResult(LoginResponseResult.UNSUCCESSFUL.ordinal());
                securityService.SaveLoginRequest(loginRequest);
                throw new SecurityServiceException(BusinessExceptionCode.ACCESS_DENIED.name());
            }
        } else {
            loginRequest.setResult(LoginResponseResult.UNSUCCESSFUL.ordinal());
            securityService.SaveLoginRequest(loginRequest);
            throw new SecurityServiceException(BusinessExceptionCode.ACCESS_DENIED.name());
        }

        Set<String> roles = null;
        if(user.getRoleList() != null && !user.getRoleList().isEmpty())
            roles = user.getRoleList().stream().map(SecurityRole::getName).collect(Collectors.toSet());
        Set<String> permissions = new HashSet<>();
        if(user.getPermissionList() != null && !user.getPermissionList().isEmpty())
            permissions = user.getPermissionList().stream().map(SecurityPermission::getName).collect(Collectors.toSet());

        Map<String, Set<String>> globalPermsMap = permissionManagerService.findPermissionsOfRoles();
        if(roles != null && !roles.isEmpty()) {
            for (String r : roles) {
                permissions.addAll(globalPermsMap.get(r));
            }
        }
        loginRequest.setResult(LoginResponseResult.SUCCESS.ordinal());
        securityService.SaveLoginRequest(loginRequest);
        return new SecurityWrapper(username, permissions, roles, null, true);
    }

    public SecurityWrapper authenticate(String jwt) {
        return JWTUtil.getSecurityWrapperFromToken(jwt);
    }

    @Transactional
    public SecurityUser registerUser(RegisterDto registerDto) {
        SecurityUser securityUser = new SecurityUser();
        securityUser.setUsername(registerDto.getUsername());

        securityUser.setPassword(passwordEncoder.encode(securityUser.getUsername()
                + securityUser.getPassword()));
        securityUser.setEmail(registerDto.getEmail());
        securityUser.setMobile(registerDto.getMobileNumber());
        securityUser.setCreationDate(new Date());
        securityUser.setStatus(1);

        List<SecurityRoleDto> roleList = roleService.findRolesByNames(registerDto.getRoles());
        List<SecurityPermissionDto> permissionList = permissionService.findPermissionsByNames(registerDto.getPermissions());

        securityUser = userDao.save(securityUser);

        if(roleList != null && !roleList.isEmpty())
            userDao.addRolesToUser(roleList.stream().map(SecurityRoleDto::getId).collect(Collectors.toList()), securityUser.getId());
        if(permissionList != null &&  !permissionList.isEmpty())
            userDao.addPermissionsToUser(permissionList.stream().map(SecurityPermissionDto::getId).collect(Collectors.toList()), securityUser.getId());


        return securityUser;
    }

    @Transactional
    public SecurityUserDto changeOwnPass(ChangePasswordDto changePasswordDto) {
        if(changePasswordDto.getNovelPassword() == null || changePasswordDto.getNovelPassword().trim().isEmpty()) {
            throw new RuntimeException(BusinessExceptionCode.PASSWORD_IS_REQUIRED.name());
        }

        if(!changePasswordDto.getNovelPassword().matches(passwordRegex))
            throw new RuntimeException(BusinessExceptionCode.PASSWORD_IS_INVALID.name());

        SecurityUser securityUser = userDao.findUserByUsernameOrEmail(changePasswordDto.getUsername(), changePasswordDto.getEmail());
        authenticate(changePasswordDto.getUsername(), changePasswordDto.getOldPassword());
        securityUser.setPassword(passwordEncoder.encode(securityUser.getUsername()
                + securityUser.getPassword()));
        securityUser = userDao.save(securityUser);
        return SecurityUserDto.toDto(securityUser);
    }

    @Transactional
    public SecurityUserDto changeOthersPass(ChangePasswordDto changePasswordDto) {
        if(changePasswordDto.getNovelPassword() == null || changePasswordDto.getNovelPassword().trim().isEmpty()) {
            throw new RuntimeException(BusinessExceptionCode.PASSWORD_IS_REQUIRED.name());
        }

        if(!changePasswordDto.getNovelPassword().matches(passwordRegex))
            throw new RuntimeException(BusinessExceptionCode.PASSWORD_IS_INVALID.name());

        SecurityUser securityUser = userDao.findUserByUsernameOrEmail(changePasswordDto.getUsername(), changePasswordDto.getEmail());
        securityUser.setPassword(passwordEncoder.encode(securityUser.getUsername()
                + securityUser.getPassword()));
        securityUser = userDao.save(securityUser);
        return SecurityUserDto.toDto(securityUser);
    }

    @Transactional
    public SecurityUserDto updateProfile(SecurityUserDto dto) {
        SecurityUser securityUser = userDao.findByPrimaryKey(dto.getId());
        securityUser.setEmail(dto.getEmail());
        securityUser.setMobile(dto.getMobile());
        securityUser.setStatus(dto.getStatus());
        return SecurityUserDto.toDto(userDao.save(securityUser));
    }
}
