package com.ibm.sso.service;

import com.caracal.data.api.GenericEntityDAO;
import com.ibm.sso.common.BusinessExceptionCode;
import com.ibm.sso.dao.SecurityPermissionDao;
import com.ibm.sso.dao.SecurityRoleDao;
import com.ibm.sso.dto.SecurityPermissionDto;
import com.ibm.sso.dto.SecurityRoleDto;
import com.ibm.sso.model.SecurityPermission;
import com.ibm.sso.model.SecurityRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SecurityRoleService extends GeneralServiceImpl<SecurityRoleDto, SecurityRole, Long> {


    private final SecurityRoleDao securityRoleDao;
    private final SecurityPermissionDao securityPermissionDao;

    @Autowired
    public SecurityRoleService(SecurityRoleDao securityRoleDao, SecurityPermissionDao securityPermissionDao) {
        this.securityRoleDao = securityRoleDao;
        this.securityPermissionDao = securityPermissionDao;
    }

    @Override
    protected GenericEntityDAO getDAO() {
        return securityRoleDao;
    }

    @Override
    public SecurityRoleDto getDtoInstance() {
        return new SecurityRoleDto();
    }

    public List<SecurityPermissionDto> findUnAssignedPermissionsForRole(Long roleId) {

        List<SecurityPermission> permissionList = securityRoleDao.findUnAssinedPermissions(roleId);
        if(permissionList == null || permissionList.isEmpty())
            return null;
        return permissionList.stream().map(SecurityPermissionDto::toDto).collect(Collectors.toList());
    }

    public List<SecurityRoleDto> findRolesByNames(List<String> names) {
        return SecurityRoleDto.toDto(securityRoleDao.findRolesByNames(names));
    }

    @Transactional
    public SecurityRoleDto update(SecurityRoleDto securityRole) {
        if(securityRole.getId() == null)
            throw new RuntimeException(BusinessExceptionCode.BAD_INPUT.name());
        SecurityRole existing = securityRoleDao.findByPrimaryKey(securityRole.getId());
        SecurityRole role = SecurityRoleDto.toEntity(securityRole);
        existing.setName(role.getName());
        existing.setTitle(role.getTitle());
        existing.setParent(role.getParent());
        if(role.getPermissionList() != null) {
            List<SecurityPermission> plist =
                    securityPermissionDao.findPermissionsByNames(
                            role.getPermissionList().stream().map(SecurityPermission::getName)
                                    .collect(Collectors.toList()));
            existing.setPermissionList(plist);
        }

        existing = securityRoleDao.save(existing);
        return SecurityRoleDto.toDto(existing);
    }
}
