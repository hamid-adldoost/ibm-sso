package com.ibm.sso.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ChangePasswordDto implements Serializable {

    private String username;
    private String email;
    private String oldPassword;
    private String novelPassword;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNovelPassword() {
        return novelPassword;
    }

    public void setNovelPassword(String novelPassword) {
        this.novelPassword = novelPassword;
    }
}
