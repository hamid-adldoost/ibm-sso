package com.ibm.sso.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RegisterDto implements Serializable {

    private String username;
    private String mobileNumber;
    private String password;
    private String email;
    private List<String> roles;
    private List<String> permissions;

    public RegisterDto() {
    }

    public RegisterDto(SecurityUserDto securityUserDto) {
        if(securityUserDto == null)
            return;
        this.setUsername(securityUserDto.getUsername());
        this.setEmail(securityUserDto.getEmail());
        this.setMobileNumber(securityUserDto.getMobile());
        if(securityUserDto.getPermissionList() != null && !securityUserDto.getPermissionList().isEmpty()) {
            this.setPermissions(securityUserDto.getPermissionList().stream().map(SecurityPermissionDto::getName).collect(Collectors.toList()));
        }
        if(securityUserDto.getRoleList() != null && !securityUserDto.getRoleList().isEmpty()) {
            this.setRoles(securityUserDto.getRoleList().stream().map(SecurityRoleDto::getName).collect(Collectors.toList()));
        }
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

//    @JsonIgnore
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public List<String> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<String> permissions) {
        this.permissions = permissions;
    }
}
